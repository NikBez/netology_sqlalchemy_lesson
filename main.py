from db.session import engine
from db.models import Base
from db.session import SessionLocal
from db.models import Publisher, Sale, Shop, Stock, Book

import json


class Handler:
    def __init__(self):
        self.session = SessionLocal()

    def import_from_json(self, path):
        with open(path, 'r') as test_data:
            tests_data = json.load(test_data)
        for record in tests_data:
            model = {
                'publisher': Publisher,
                'shop': Shop,
                'book': Book,
                'stock': Stock,
                'sale': Sale,
            }[record.get('model')]
            self.session.add(model(id=record.get('pk'), **record.get('fields')))
        self.commit()

    def add_publisher(self, name):
        publisher = Publisher(name)
        self.session.add(publisher)

    def add_book(self, title, id_publisher):
        book = Book(title, id_publisher)
        self.session.add(book)

    def add_shop(self, name):
        shop = Shop(name)
        self.session.add(shop)

    def add_stock(self, id_shop, id_book, count):
        stock = Stock(id_shop, id_book, count)
        self.session.add(stock)

    def add_sale(self, price, date_sale, id_stock, count):
        sale = Sale(price, date_sale, id_stock, count)
        self.session.add(sale)

    def commit(self):
        self.session.commit()

    def get_sales_by_publisher(self, publisher):
        request = self.session.query(Book.title, Shop.name, Sale.price, Sale.date_sale) \
            .select_from(Book) \
            .join(Book.publisher) \
            .join(Stock, Book.id == Stock.id_book) \
            .join(Sale, Stock.id == Sale.id_stock) \
            .join(Shop, Stock.id_shop == Shop.id) \
            .filter(Publisher.name == publisher)\
            .all()
        if not request:
            print(f'There are not sales with {publisher}')
        for book_title, shop_name, price, date_sale in request:
            formatted_date = date_sale.strftime('%d-%m-%Y')
            print(f'{book_title} | {shop_name} | {price}руб. | Дата продажи: {formatted_date}')

    def close(self):
        self.session.close()


def create_tables():
    print('Create tables!')
    Base.metadata.create_all(engine)


def start_application():
    create_tables()

    handler = Handler()
    # handler.import_from_json('tests_data.json')
    publisher = input('Enter publisher: ')
    handler.get_sales_by_publisher(publisher)
    handler.close()


if __name__ == '__main__':
    app = start_application()




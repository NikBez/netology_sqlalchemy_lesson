from datetime import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Numeric


Base = declarative_base()


class Publisher(Base):
    __tablename__ = 'publisher'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)

    def __str__(self):
        return f'Publisher {self.id}: {self.name}'


class Book(Base):
    __tablename__ = 'book'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, nullable=False)
    id_publisher = Column(Integer, ForeignKey('publisher.id'))
    publisher = relationship(Publisher, backref='books')

    def __str__(self):
        return f'Book {self.id}: {self.title}'


class Shop(Base):
    __tablename__ = 'shop'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)

    def __str__(self):
        return f'Shop {self.id}: {self.name}'


class Stock(Base):
    __tablename__ = 'stock'

    id = Column(Integer, primary_key=True)
    id_shop = Column(Integer, ForeignKey('shop.id'), nullable=False)
    id_book = Column(Integer, ForeignKey('book.id'), nullable=False)
    count = Column(Integer, default=0)
    book = relationship(Book, backref='stocks')
    shop = relationship(Shop, backref='stocks')

    def __str__(self):
        return f'Stock {self.id}: {self.book}/ {self.shop} - в количестве: {self.count}'


class Sale(Base):
    __tablename__= 'sale'

    id = Column(Integer, primary_key=True)
    price = Column(Numeric(scale=2), nullable=False)
    date_sale = Column(DateTime, nullable=False, default=datetime.utcnow)
    id_stock = Column(Integer, ForeignKey('stock.id'), nullable=False)
    stock = relationship(Stock, backref='sales')
    count = Column(Integer, nullable=False)

    def __str__(self):
        return f'Sale {self.id}: {self.date_sale}/ {self.id_stock} - в количестве: {self.count}'



